FROM python:3.8.10-slim
WORKDIR /app
ADD . /app
RUN pip install --trusted-host pypi.python.org  Flask
ENV NAME=Bilal
CMD [ "python", "app.py" ]